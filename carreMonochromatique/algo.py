import random as r

N = 100
D = [[r.randint(1, 3) for _ in range(N)] for _ in range(N)]

# search square chromatic dim = 2
#
# D = [
#     [1, 2, 2, 1],
#     [1, 2, 2, 1],
#     [3, 3, 3, 3],
#     [0, 0, 0, 0]
# ]

# N = len(D)

# Afficher la matrice
show = True

if show:
    for line in D:
        for num in line:
            print(num, end="")
        print()

"""
Explore si en fonction de l la dimension du carré celui-ci est chromatique
"""


def explore(a, b, Matrix, z, dim):

    for y in range(dim):
        if D[a][b] != z or Matrix[a][b + y] != z or Matrix[a + y][b] != z or Matrix[a + y][b + y] != z:
            return False

    return True

def squareChromatic(D, l):
    subMatrix = []
    for a in range(N):
        for b in range(N):
            # z valeur constante dans le carré
            z = D[a][b]
            isChromatic = True

            # condition pour tester si on peut trouver un carré chromatic
            if a + l - 1 < N and b + l - 1 < N:
                isChromatic = explore(a, b, D, z, l)
            else:
                isChromatic = False

            if isChromatic:
                subMatrix.append({l: ((a, b), l)})

    return subMatrix


for l in range(2, N + 1):
    sc = squareChromatic(D, l)
    if len(sc) > 0:
        print(sc)
